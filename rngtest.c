/****************************************************************************
 * apps/system/rngtest/rngtest.c
 *
 *   Copyright (C) 2016 Sebastien Lorquet. All rights reserved.
 *   Author: Sebastien Lorquet <sebastien@lorquet.fr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <nuttx/progmem.h>

#include <sys/types.h>

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sched.h>
#include <syslog.h>
#include <errno.h>
#include <wchar.h>
#include <math.h>

#include <nuttx/usb/usbdev_trace.h>

#define USE32

#ifdef CONFIG_SYSTEM_RNGTEST

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

#define RAMTEST_PREFIX "RAMTest: "

/****************************************************************************
 * Private Types
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

#ifdef USE32
#define count_t uint32_t
#define flt_t   float
#define LOG(f)  log2f(f)
#define STEP    10000
#else
#define count_t uint64_t
#define flt_t   double
#define LOG(d)  log2(d)
#define STEP    5000
#endif

count_t nsamples;      //number of bytes generated
count_t distribs[256]; //count the occurrence of each value
count_t total;
count_t prevtotal;
flt_t   average;
flt_t   entropy;

int      devrandom;
uint8_t  buffer[1024];

/****************************************************************************
 * Private Functions
 ****************************************************************************/


/****************************************************************************
 * Public Functions
 ****************************************************************************/

#ifdef CONFIG_BUILD_KERNEL
int main(int argc, FAR char *argv[])
#else
int rngtest_main(int argc, char **argv)
#endif
{
  char b;
  int oflags;
  int ret;
  uint32_t i;
  uint32_t done = 0;

  /* Set the host stdin to O_NONBLOCK */

  oflags = fcntl(0, F_GETFL, 0);
  if (oflags == -1)
    {
      fprintf(stderr, "ERROR: fnctl(F_GETFL) failed: %d\n", errno);
      return 6;
    }

  ret = fcntl(0, F_SETFL, oflags | O_NONBLOCK);
  if (ret < 0)
    {
      fprintf(stderr, "ERROR: fnctl(F_SETFL) failed: %d\n", errno);
      return 7;
    }

  fflush(stdin);

  printf("Generating random data, press ctrl-C to stop...\n");

  devrandom = open("/dev/random", O_RDONLY);


again:
  nsamples = 0;
  total = 0;
  for(i=0; i<256; i++)
    {
      distribs[i] = 0;
    }

  while(true)
    {
      //get random bytes
      if(read(devrandom, buffer, sizeof(buffer)) != sizeof(buffer))
        {
          printf("Read error on /dev/random, errno=%d\n",errno);
          break;
        }
      //Statistics computation
      nsamples += sizeof(buffer);
      done     += sizeof(buffer);
      prevtotal = total;

      for(i=0; i<sizeof(buffer); i++)
        {
          total += buffer[i];
          distribs[buffer[i]] += 1;
        }

      if(total<prevtotal)
        {
          printf("accumulation overflow, restart\n");
          goto again;
        }

      average = (flt_t)total / (flt_t)nsamples;

      entropy = 0;
      for(i=0; i<256; i++)
        {
        flt_t prob = (flt_t)distribs[i] / (flt_t)nsamples;
        flt_t logp = LOG(prob);
        //printf("%u %lu %f %f\n", i, distribs[i], prob, logp);
        if(prob>0)
          {
            entropy -= prob * logp;
          }
        }

      //Display
      if(done>STEP)
        {
#ifdef USE32
          printf("cnt %lu avg %f ent %f\n", nsamples, average, entropy);
          //printf("avg %f\n", average);
#else
          printf("cnt %llu avg %lf ent %lf\n", nsamples, average, entropy);
          //printf("cnt %llu\n", nsamples);
          //printf("avg %lf\n", average);
#endif
          done=0;
        }

      //stop on keypress
      ret = read(0, &b, 1);
      if(ret > 0 && b == 0x03)
        {
          break;
        }
    }

  close(devrandom);
  printf("\nDone.\n");

  oflags = fcntl(0, F_GETFL, 0);
  if (oflags == -1)
    {
      fprintf(stderr, "ERROR: fnctl(F_GETFL) failed: %d\n", errno);
      return 6;
    }

  ret = fcntl(0, F_SETFL, oflags & ~O_NONBLOCK);
  if (ret < 0)
    {
      fprintf(stderr, "ERROR: fnctl(F_SETFL) failed: %d\n", errno);
      return 7;
    }

  return 0;
}

#endif /* CONFIG_SYSTEM_RNGTEST */
