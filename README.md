# README #

This nuttx application tests a random hardware generator. it computes average value and entropy.

### Installation ###

* clone a working nuttx environment
* go to apps/system
* git clone this repository
* edit apps/system/Kconfig to include the repository directory
* go to nuttx/
* make menuconfig
* select application system/rngtest
* build
* enjoy